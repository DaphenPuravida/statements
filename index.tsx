import * as React from "react";
import { InvoiceApp } from "./App";
import ReactDom from "react-dom";

ReactDom.render(<InvoiceApp />, document.getElementById("root"));
