import * as React from "react";
import "./app.css";

export const InvoiceApp = () => {
  return (
    <div>
      <div className="titleBar">
        <div className="logo">EVEREST</div>
        <div className="contact">
          <div>www.everestcard.com</div>
          <div>+352 203 31027</div>
        </div>
      </div>

      <div className="dateBar">
        <div className="monthly">
          Monthly statement April 10, 2020 - May 9, 2020
        </div>
        <div className="page">Page 1 of 1</div>
      </div>

      <div className="customerAmmountBar">
        <div className="companyInfo">
          <div>
            <b>Customer Company SA</b>
          </div>
          <div>100, Boulevard Royal, Brussels, Belgium</div>
          <div>IBAN: LUXXXXXXXX</div>
          <div>BIC: XXXXXX</div>
        </div>

        <div>
          <div className="totalAmount">
            <div>
              <div>Total amount spent:</div>
              <div>Total amount received:</div>
            </div>
            <div className="amount">
              <div>€4,135.00</div>
              <div>€1,500.60</div>
            </div>
          </div>
          <div className="totalDue">
            <div>
              <div>Total amount due:</div>
              <div>Please pay no later then:</div>
            </div>
            <div className="amount">
              <div>€ 2,684.00</div>
              <div>May 15th, 2020</div>
            </div>
          </div>
        </div>
      </div>

      <div className="cardTransactions">
        Card Transactions: Denis Kiselev ( xx 3254, xx 7657, xx 3423, xx 1212 )
      </div>
    </div>
  );
};
